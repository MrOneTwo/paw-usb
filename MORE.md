So you want to know more? Keep on reading.

## Architecture

The main design of the hardware polling and sending data via USB to the host is that Systick fires
at 100Hz every N-th Systick the controller's consensus state is pushed to the host. Between those
N-th Systicks, Systicks just poll the controller's state. That way in between those N-th Systicks
a collection of controller states is gathered. That adds debouncing.


## Development

### Flashing/debugging

When using ST-Link programmer:

```sh
st-flash write binary.bin 0x08000000
```

#### Flash with *OpenOCD*

Start *OpenOCD* server:

```sh
openocd -f interface/stlink-v2.cfg -f target/stm32f1x.cfg
```

Connect to the server (port 4444 is for CLI interface):

```sh
telnet localhost 4444
```

Load the binary (using *OpenOCD* you need to use the `elf` file):

```
reset halt
flash write_image erase binary.elf
reset
```

#### Flash via **GBD**

This needs to have *OpenOCD* server running.

Simple way (port `3333` is for `gdb` connections):

```sh
arm-none-eabi-gdb -ex 'target remote localhost:3333' -ex 'load' src/binary.elf
```

You can just start the `gdb` manually:

```
file src/pawusb.elf
target extended-remote : 3333
load
```
