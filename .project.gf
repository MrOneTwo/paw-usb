[commands]
Compile=shell gcc -o main -ggdb main.c
Set breakpoints=b main;b printf

[gdb]
path=./toolchain/gdb

[pipe]
control=./gf_piperino
