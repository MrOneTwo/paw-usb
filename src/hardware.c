#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>

#include "printf.h"
#include "hardware.h"

#define CONTROLLER_STATE_ARRAY_SIZE (2U)

// This is a collection of the controller states.
static Controller_t controller_states[CONTROLLER_STATE_ARRAY_SIZE];

static Controller_t controller_consensus;
static Controller_t controller_consensus_prev;

static uint8_t controller_state_tail_index;


static void _hw_setup_gpio(void)
{
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOB);
  rcc_periph_clock_enable(RCC_GPIOC);

  // Setup LED pin.
  gpio_set(GPIOC, GPIO13);
  gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);

  // Setup buttons pins.
  gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN,
    GPIO4 | GPIO5 | GPIO6 | GPIO7 | GPIO8 | GPIO9 | GPIO10 | GPIO11);
  // To select down or up the ODR register needs to be programmed via BSRR register.
  // gpio_clear selects pull down, gpio_set selects pull up.
  gpio_clear(GPIOB,
    GPIO4 | GPIO5 | GPIO6 | GPIO7 | GPIO8 | GPIO9 | GPIO10 | GPIO11);

  // Setting input pins for the encoder. Pulling up via gpio_set makes it stable.
  // Floating is unstable and pull down makes it not work. Maybe pulling up externally
  // and making them float is another way to do it.
  gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO0 | GPIO1);
  gpio_set(GPIOA, GPIO0 | GPIO1);
}

static void _hw_setup_encoder(void)
{
  rcc_periph_clock_enable(RCC_TIM2);

  timer_set_period(TIM2, 1024);
  timer_slave_set_mode(TIM2, TIM_SMCR_SMS_EM3); // encoder
  timer_ic_set_input(TIM2, TIM_IC1, TIM_IC_IN_TI1);
  timer_ic_set_input(TIM2, TIM_IC2, TIM_IC_IN_TI2);
  timer_enable_counter(TIM2);
}

void hw_init_perphs(void)
{
  _hw_setup_gpio();
  _hw_setup_encoder();
}

void hw_controller_state_string(char* buf, uint32_t buf_len, Controller_t ct)
{
  snprintf(buf, buf_len,
    "\r\n"
    " %d\r\n"
    "[%d %d %d %d %d %d %d %d\r\n"
    " %d %d %d %d %d %d %d %d\r\n"
    " %d %d %d %d %d %d %d %d\r\n"
    " %d %d %d %d %d %d %d %d]"
    , ct.encoder_pos
    , ct.bit_buttons.b0, ct.bit_buttons.b1, ct.bit_buttons.b2, ct.bit_buttons.b3
    , ct.bit_buttons.b4, ct.bit_buttons.b5, ct.bit_buttons.b6, ct.bit_buttons.b7
    , ct.bit_buttons.b8, ct.bit_buttons.b9, ct.bit_buttons.b10, ct.bit_buttons.b11
    , ct.bit_buttons.b12, ct.bit_buttons.b13, ct.bit_buttons.b14, ct.bit_buttons.b15
    , ct.bit_buttons.b16, ct.bit_buttons.b17, ct.bit_buttons.b18, ct.bit_buttons.b19
    , ct.bit_buttons.b20, ct.bit_buttons.b21, ct.bit_buttons.b22, ct.bit_buttons.b23
    , ct.bit_buttons.b24, ct.bit_buttons.b25, ct.bit_buttons.b26, ct.bit_buttons.b27
    , ct.bit_buttons.b28, ct.bit_buttons.b29, ct.bit_buttons.b30, ct.bit_buttons.b31
  );
}

void hw_save_new_controller_state(void)
{
  // This shouldn't fire. That's because the hw_establish_consensus_state() should
  // reset the controller_state_tail_index to 0. If I forget to do that, then it
  // will try to write into array out of bounds.
  if (controller_state_tail_index >= CONTROLLER_STATE_ARRAY_SIZE)
  {
    controller_state_tail_index = 0U;
  }

  uint16_t buttons_B = gpio_get(GPIOB,
    GPIO4 | GPIO5 | GPIO6 | GPIO7 | GPIO8 | GPIO9 | GPIO10 | GPIO11);

  Controller_t ct = {};
  ct.buttons = buttons_B;

  // TODO(michalc): temporary hack. The pins are pulled low so the buttons' state
  // are reversed.
  ct.bit_buttons.b4 = ct.bit_buttons.b4 ? 0 : 1;
  ct.bit_buttons.b5 = ct.bit_buttons.b5 ? 0 : 1;
  ct.bit_buttons.b6 = ct.bit_buttons.b6 ? 0 : 1;
  ct.bit_buttons.b7 = ct.bit_buttons.b7 ? 0 : 1;
  ct.bit_buttons.b8 = ct.bit_buttons.b8 ? 0 : 1;
  ct.bit_buttons.b9 = ct.bit_buttons.b9 ? 0 : 1;
  ct.bit_buttons.b10 = ct.bit_buttons.b10 ? 0 : 1;
  ct.bit_buttons.b11 = ct.bit_buttons.b11 ? 0 : 1;

  ct.encoder_pos = timer_get_counter(TIM2);

  controller_states[controller_state_tail_index] = ct;

  controller_state_tail_index += 1;
}

void hw_establish_consensus_state(void)
{
  // Save the last consensus state.
  controller_consensus_prev = controller_consensus;

  // Establish new consensus by first combining the states between the
  // systicks.
  Controller_t ct;
  ct.buttons = 0xFFFFFFFF;
  // The encoder position comes from the last saved state.
  ct.encoder_pos = controller_states[controller_state_tail_index - 1].encoder_pos;
  for(uint8_t i = 0; i < controller_state_tail_index; i++)
  {
    ct.buttons &= controller_states[i].buttons;
  }
  controller_consensus = ct;

  // After we are done, resset the tail index to the beginning of the array.
  controller_state_tail_index = 0U;
}

uint8_t hw_state_changed(void)
{
  if (controller_consensus.buttons != controller_consensus_prev.buttons)
  {
    return 1;
  }

  if (controller_consensus.encoder_pos != controller_consensus_prev.encoder_pos)
  {
    return 1;
  }

  return 0;
}

Controller_t hw_get_consensus_state(void)
{
  return controller_consensus;
}

