#ifndef KEYMAP_H
#define KEYMAP_H

#include "config.h"

#include <stdint.h>

typedef enum Button_Role_e
{
  KEY, MODIFIER, MACRO
} Button_Role_e;

typedef struct Button_Function_t
{
  Button_Role_e role;
  union {
    uint8_t key;
    uint8_t modifiers_mask;
  };
} Button_Function_t;

extern Button_Function_t keymap[CFG_BUTTONS_COUNT];

#endif // KEYMAP_H
