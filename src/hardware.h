#ifndef HARDWARE_H
#define HARDWARE_H

typedef struct _Buttons_t {
  uint32_t b0 : 1; uint32_t b1 : 1; uint32_t b2 : 1; uint32_t b3 : 1;
  uint32_t b4 : 1; uint32_t b5 : 1; uint32_t b6 : 1; uint32_t b7 : 1;
  uint32_t b8 : 1; uint32_t b9 : 1; uint32_t b10 : 1; uint32_t b11 : 1;
  uint32_t b12 : 1; uint32_t b13 : 1; uint32_t b14 : 1; uint32_t b15 : 1;
  uint32_t b16 : 1; uint32_t b17 : 1; uint32_t b18 : 1; uint32_t b19 : 1;
  uint32_t b20 : 1; uint32_t b21 : 1; uint32_t b22 : 1; uint32_t b23 : 1;
  uint32_t b24 : 1; uint32_t b25 : 1; uint32_t b26 : 1; uint32_t b27 : 1;
  uint32_t b28 : 1; uint32_t b29 : 1; uint32_t b30 : 1; uint32_t b31 : 1;
} Buttons_t;

typedef struct _Controller_t {
  uint32_t encoder_pos;
  union {
    Buttons_t bit_buttons;
    uint32_t buttons;
  };
} Controller_t;

/*
 * ...
 */
void hw_init_perphs(void);

/*
 * Retrieve a string representation of the consensus state.
 */
void hw_controller_state_string(char* buf, uint32_t buf_len, Controller_t c);

/*
 * Save a new controller state in an array of states.
 */
void hw_save_new_controller_state(void);

/*
 * When the array of states is full (or less), a consensus state should
 * be established.
 */
void hw_establish_consensus_state(void);

/*
 * Check if the previous consensus state and the current consensus state
 * differ.
 */
uint8_t hw_state_changed(void);

/*
 * Get the consensus state.
 */
Controller_t hw_get_consensus_state(void);


#endif // HARDWARE_H
