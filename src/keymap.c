#include "config.h"
#include "keycodes.h"

#include "keymap.h"

// 1x4
// 1x3
// 2x1
// 2x2
// 2x4
// 2x3
// 1x1
// 1x2

// TODO(michalc): size of this needs to be synced.
Button_Function_t keymap[CFG_BUTTONS_COUNT] = {
  {.role = MODIFIER, .modifiers_mask = KEYBD_MOD_SHIFT_LEFT_MASK},
  {.role = MODIFIER, .modifiers_mask = KEYBD_MOD_CTRL_LEFT_MASK},
  {.role = MODIFIER, .modifiers_mask = KEYBD_MOD_ALT_LEFT_MASK},
  {.role = KEY, .key = KEYBD_C},
  {.role = KEY, .key = KEYBD_F},
  {.role = KEY, .key = KEYBD_B},
  {.role = KEY, .key = KEYBD_S},
  {.role = KEY, .key = KEYBD_B},
};
