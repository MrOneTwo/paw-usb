#include <libopencm3/cm3/itm.h>
#include <libopencm3/cm3/memorymap.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/scs.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/cm3/tpiu.h>
#include <libopencm3/stm32/dbgmcu.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/i2c.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/spi.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/usb/dwc/otg_fs.h>
#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/hid.h>

#include "printf.h"
#include "hardware.h"
#include "keymap.h"
#include "keycodes.h"
#include "config.h"
#include "version.h"

#include <string.h>

void _putchar(char c)
{
  (void)c;
}

uint32_t str_len(const char *str)
{
  return (*str) ? str_len(++str) + 1 : 0;
}

#define TRACE_PRINT(port, string) \
{ \
  char* msg __attribute__((aligned(4))) = string; \
  trace_write_str(port, msg, str_len(msg)); \
}

#define PRINT_BUF_SIZE (128)
char print_buf[PRINT_BUF_SIZE];

/*
static void itm_init(void)
{
  // Enable trace subsystem (ITM and TPIU).
  SCS_DEMCR |= SCS_DEMCR_TRCENA;

  // NRZ encoding code for async transmission.
  TPIU_SPPR = TPIU_SPPR_ASYNC_NRZ;

  // Formatter and flash control.
  TPIU_FFCR &= ~TPIU_FFCR_ENFCONT;

  // NOTE(michalc): the reference manual (p.1103) says that the TRACE_IOEN and TRACE_MODE
  // should be set by the host side debugger. Does that mean you can't fully configure
  // ITM without starting OpenOCD?
  // Enable TRACESWO pin for async mode (no clock pin).
  DBGMCU_CR = DBGMCU_CR_TRACE_IOEN | DBGMCU_CR_TRACE_MODE_ASYNC;

  // Unlock access to the ITM registers. You can't configure them without first
  // unlocking the access.
  ITM_LAR = CORESIGHT_LAR_KEY;

  // Set the source ID to 1 (7 bits 16-22 encode ID) and enable ITM.
  ITM_TCR = (1 << 16) | ITM_TCR_ITMENA;
  // Enable the stimulus port.
  const uint8_t port = 0;
  ITM_TER[0] = (1 << port);
}
*/

static void trace_write_char(const uint8_t port, const char c)
{
  // Check if the port is enabled.
  if (!(ITM_TER[0] & (1 << port)))
  {
    return;
  }
  while (!(ITM_STIM8(port) & ITM_STIM_FIFOREADY));
  ITM_STIM8(port) = c;
}

static void trace_write_str(const uint8_t port, const char* const s, uint32_t s_len)
{
  for (uint32_t i = 0; i < s_len; i++)
  {
    trace_write_char(port, *(s + i));
  }
}


////////////////////////
//
//  USB
//

#define ENDPOINT_ADDRESS (0x81)

typedef enum Usb_state_e {
  USB_UNINITILIZED = 0,
  USB_INITILIZED = 1,
} Usb_state_e;

Usb_state_e usb_state;

static usbd_device *usbd_dev;

// Explanation of the decriptors below.
// http://www.usbmadesimple.co.uk/ums_5.htm
const struct usb_device_descriptor dev_descr = {
  .bLength = USB_DT_DEVICE_SIZE,     //
  .bDescriptorType = USB_DT_DEVICE,  //
  .bcdUSB = 0x0200,                  // spec version
  .bDeviceClass = 0,                 // class information in interface descriptor
  .bDeviceSubClass = 0,              // class information in interface descriptor
  .bDeviceProtocol = 0,              // class information in interface descriptor
  .bMaxPacketSize0 = 64,             // either 8, 16, 32, 64
  .idVendor = 0xfeed,                // Planck VID
  .idProduct = 0x6060,               // Planck PID
  .bcdDevice = 0x0200,               // device release number
  .iManufacturer = 1,                // index to manufacturer string
  .iProduct = 2,                     // index to product string
  .iSerialNumber = 3,                // index to serial number
  .bNumConfigurations = 1,           // number of possible configurations
};

static const uint8_t hid_report_descriptor[] = {
  0x05, 0x01, /* USAGE_PAGE (Generic Desktop)       */

  0x09, 0x06, /* USAGE (Mouse)                      */
  0xa1, 0x01, /* COLLECTION (Application)           */
  0x85, 0x01, /*   REPORT_ID (1)                    */
              /*   Buttons                          */
  0x05, 0x09, /*   USAGE_PAGE (Buttons)             */
  0x19, 0x01, /*   USAGE_MINIMUM ()                 */
  0x29, 0x03, /*   USAGE_MAXIMUM ()                 */
  0x15, 0x00, /*   LOGICAL_MINIMUM (0)              */
  0x25, 0x01, /*   LOGICAL_MAXIMUM (1)              */
  0x95, 0x03, /*   REPORT_COUNT (3)                 */
  0x75, 0x01, /*   REPORT_SIZE (1)                  */
  0x81, 0x02, /*   INPUT (Data,Variable,Absolute)   */
  0x95, 0x01, /*   REPORT_COUNT (1)                 */
  0x75, 0x05, /*   REPORT_SIZE (5)                  */
  0x81, 0x01, /*   INPUT (Constant)                 */
              /*   Pointer                          */
  0x05, 0x01, /*   USAGE_PAGE (Generic Desktop)     */
  0x09, 0x30, /*   USAGE (X)                        */
  0x09, 0x31, /*   USAGE (Y)                        */
  0x15, 0x81, /*   LOGICAL_MINIMUM (-127)           */
  0x25, 0x7f, /*   LOGICAL_MAXIMUM (127)            */
  0x95, 0x02, /*   REPORT_COUNT (2)                 */
  0x75, 0x08, /*   REPORT_SIZE (8)                  */
  0x81, 0x06, /*   INPUT (Data,Variable,Relative)   */
  0xc0,       /* END_COLLECTION                     */

  0x09, 0x06, /* USAGE (Keyboard)                   */
  0xa1, 0x01, /* COLLECTION (Application)           */
  0x85, 0x02, /*   REPORT_ID (2)                    */
              /*   Modifier keys                    */
  0x05, 0x07, /*   USAGE_PAGE (Keyboard)            */
  0x19, 0xe0, /*   USAGE_MINIMUM (LCtrl)            */
  0x29, 0xe7, /*   USAGE_MAXIMUM (RGui)             */
  0x15, 0x00, /*   LOGICAL_MINIMUM (0)              */
  0x25, 0x01, /*   LOGICAL_MAXIMUM (1)              */
  0x95, 0x08, /*   REPORT_COUNT (8)                 */
  0x75, 0x01, /*   REPORT_SIZE (1)                  */
  0x81, 0x02, /*   INPUT (Data,Variable,Absolute)   */
              /*   Reserved byte                    */
  0x95, 0x08, /*   REPORT_COUNT (8)                 */
  0x75, 0x01, /*   REPORT_SIZE (1)                  */
  0x81, 0x01, /*   INPUT (Constant)                 */
              /*   Keyboard buttons                 */
  0x05, 0x07, /*   USAGE_PAGE (Keyboard)            */
  0x19, 0x00, /*   USAGE_MINIMUM (Reserved - no event indicated) */
  0x29, 0x65, /*   USAGE_MAXIMUM (Keyboard Application) */
  0x15, 0x00, /*   LOGICAL_MINIMUM (0)              */
  0x25, 0x7f, /*   LOGICAL_MAXIMUM (101)            */
  0x95, 0x06, /*   REPORT_COUNT (6)                 */
  0x75, 0x08, /*   REPORT_SIZE (8)                  */
  0x81, 0x00, /*   INPUT (Data,Array,Absolute)      */
  0xc0        /* END_COLLECTION                     */
};


// Create an anonymous struct and create a hid_function variable
// out of that and initilize it with a default values.
static const struct {
  struct usb_hid_descriptor hid_descriptor;
  struct {
    uint8_t bReportDescriptorType;
    uint16_t wDescriptorLength;
  } __attribute__((packed)) hid_report;
} __attribute__((packed)) hid_function =
{
  .hid_descriptor = {
    .bLength = sizeof(hid_function),
    .bDescriptorType = USB_DT_HID,
    .bcdHID = 0x0100,
    .bCountryCode = 0,
    .bNumDescriptors = 1,
  },

  .hid_report = {
    .bReportDescriptorType = USB_DT_REPORT,
    .wDescriptorLength = sizeof(hid_report_descriptor),
  }
};

const struct usb_endpoint_descriptor hid_endpoint = {
  .bLength = USB_DT_ENDPOINT_SIZE,
  .bDescriptorType = USB_DT_ENDPOINT,
  .bEndpointAddress = ENDPOINT_ADDRESS,   // bit 7 is direction 0 = OUT, 1 = IN
  .bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,
  .wMaxPacketSize = 16,                   //
  .bInterval = 0x1e,                      // polling interval in miliseconds (0x1e = 30 ms)
  .extra = (void*)0,
  .extralen = 0,
};

const struct usb_interface_descriptor hid_iface = {
  .bLength = USB_DT_INTERFACE_SIZE,
  .bDescriptorType = USB_DT_INTERFACE,
  .bInterfaceNumber = 0,
  .bAlternateSetting = 0,
  .bNumEndpoints = 1,
  .bInterfaceClass = USB_CLASS_HID,
  .bInterfaceSubClass = 0,
  .bInterfaceProtocol = 0,
  .iInterface = 0,

  .endpoint = &hid_endpoint,

  .extra = &hid_function,
  .extralen = sizeof(hid_function),
};

const struct usb_interface ifaces[] = {
  {
    .num_altsetting = 1,
    .altsetting = &hid_iface,
  }
};

const struct usb_config_descriptor config = {
  .bLength = USB_DT_CONFIGURATION_SIZE,
  .bDescriptorType = USB_DT_CONFIGURATION,
  .wTotalLength = 0,
  .bNumInterfaces = sizeof(ifaces)/sizeof(ifaces[0]),
  .bConfigurationValue = 1,
  .iConfiguration = 0,
  .bmAttributes = 0xC0,
  .bMaxPower = 0x32,

  .interface = ifaces,
};

static const char *usb_strings[] = {
  "Curious Technologies",
  "Paw",
  "USB version",
};

/* Buffer used for control requests. */
static uint8_t usbd_control_buffer[128];

static enum usbd_request_return_codes
hid_control_request(usbd_device *dev,
                    struct usb_setup_data *req,
                    uint8_t **buf, uint16_t *len,
                    void (**complete)(usbd_device *dev, struct usb_setup_data *req))
{
  TRACE_PRINT(0, "hid_control_request\r\n");
  (void)complete;
  (void)dev;

  if((req->bmRequestType != 0x81) ||
     (req->bRequest != USB_REQ_GET_DESCRIPTOR) ||
     (req->wValue != 0x2200))
    return USBD_REQ_NOTSUPP;

  // Point to the HID report descriptor.
  *buf = (uint8_t *)hid_report_descriptor;
  *len = sizeof(hid_report_descriptor);

  systick_counter_enable();
  usb_state = USB_INITILIZED;
  return USBD_REQ_HANDLED;
}

static void hid_set_config(usbd_device *dev, uint16_t wValue)
{
  (void)wValue;
  (void)dev;

  usbd_ep_setup(dev, ENDPOINT_ADDRESS, USB_ENDPOINT_ATTR_INTERRUPT, 4, NULL);

  usbd_register_control_callback(
        dev,
        USB_REQ_TYPE_STANDARD | USB_REQ_TYPE_INTERFACE,
        USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
        hid_control_request);
}

//
//  USB - end
//
////////////////////////

static uint32_t systick_counter = 0;

static void setup_clock(void)
{
  // usbd.h says 'It is required that the 48MHz USB clock is already available.'
  rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);

  systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);
  // SysTick interrupt every N clock pulses: set reload to N-1
  // 72MHz / 8 = 9MHz
  // 9000000Hz / 90000 = 100Hz
  systick_set_reload(89999);
  systick_interrupt_enable();
}

// TODO(michalc): this is trash because the timer is used for the encoder in the hardware.c.
// Don't touch TIM2.
/*
static void setup_tim2(void)
{
  rcc_periph_clock_enable(RCC_TIM2);
  nvic_enable_irq(NVIC_TIM2_IRQ);
  // SysTick has a default priority level that is higher than any interrupt, but that's only
  // because at reset, all priorities are set to 0, and in case of a tie, the lower IRQ
  // number wins, in this case, SysTick with -1.
  // The priorities are in steps of 16.
  nvic_set_priority(NVIC_TIM2_IRQ, 255 - 16 * 4);
  // Reset TIM2 to defaults.
  rcc_periph_reset_pulse(RST_TIM2);
  // No divider, alignment edge, direction down.
  timer_set_mode(TIM2, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_DOWN);

  timer_disable_preload(TIM2);

  // Clock source is SYSCLK -> AHB precaler -> APB1 prescaler (output limited to 36Mhz).
  // You can use the rcc_ahb_frequency, rcc_apb1_frequency variables from rcc.h.
  // Then there is the timer's prescaler (value between 1 - 65536).
  // clk_freq / prescaler = target_frequency
  // If the APB1 prescaler is 1 then the timer freq is x1 else it's x2.
  // Datasheet page 126.
  const uint32_t timer_frequency = rcc_get_timer_clk_freq(TIM2);

  // Set the target frequency to 5kHz.
  const uint32_t timer_target_frequency = 5000;
  const uint32_t timer_prescaler = (rcc_apb1_frequency * 2) / timer_target_frequency;
  timer_set_prescaler(TIM2, timer_prescaler);
  snprintf(print_buf, PRINT_BUF_SIZE,
           "Initing timer 2 with freq: %d, prescaler: %d\r\n",
           timer_frequency, timer_prescaler);
  TRACE_PRINT(0, print_buf);

  timer_one_shot_mode(TIM2);

  // The timer is 16 bit. This is an auto-reload register.
  timer_set_period(TIM2, 65535);

  // Set the compare value for OC1. 99 should result in 5Hz per 1/100s.
  // That would be 5Hz per 100Hz Systick.
  timer_set_oc_value(TIM2, TIM_OC1, 99);

  // Enable compare interrupt, channel 1.
  timer_enable_irq(TIM2, TIM_DIER_CC1IE);

  timer_enable_counter(TIM2);
}
*/

int main(void)
{
  // itm_init();
  // If you want to trace before setting up the clock you need to change
  // OpenOCD's core frequency to 8000000.
  setup_clock();
  hw_init_perphs();
  snprintf(print_buf, PRINT_BUF_SIZE, "\r\n--- PAW USB %s ---\r\n", FIRMWARE_VERSION);
  TRACE_PRINT(0, print_buf);

  ////////////////////////
  //
  //  USB (PA12 - D+, PA11 - D-)
  //

  /*
   * This is a somewhat common cheap hack to trigger device re-enumeration
   * on startup.  Assuming a fixed external pullup on D+, (For USB-FS)
   * setting the pin to output, and driving it explicitly low effectively
   * "removes" the pullup.  The subsequent USB init will "take over" the
   * pin, and it will appear as a proper pullup to the host.
   * The magic delay is somewhat arbitrary, no guarantees on USBIF
   * compliance here, but "it works" in most places.
   */
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO12);
  gpio_clear(GPIOA, GPIO12);
  // Delay the enumeration - this works.
  for (uint32_t i = 0; i < 0x800000; i++)
  {
    __asm__("nop");
  }

  usbd_dev = usbd_init(&st_usbfs_v1_usb_driver,
                       &dev_descr,
                       &config,
                       usb_strings, sizeof(usb_strings)/sizeof(char*),
                       usbd_control_buffer, sizeof(usbd_control_buffer));

  // TODO(michalc): look into other usbd_register_ functions.
  usbd_register_set_config_callback(usbd_dev, hid_set_config);

  //
  //  USB - end
  //
  ////////////////////////

  while (1)
  {
    usbd_poll(usbd_dev);
  }

}



// Reflect the USB HID report in form of a struct.
struct Composite_Report_t {
  uint8_t report_id;
  union {
    struct {
      uint8_t buttons;
      uint8_t x;
      uint8_t y;
    } __attribute__((packed)) mouse;

    struct{
      uint8_t modifiers;
      uint8_t reserved;
      uint8_t keys_down[6];
      uint8_t leds;
    } __attribute__((packed)) keyboard;
  };
} __attribute__((packed));
typedef struct Composite_Report_t Composite_Report_t;


void controller_state_to_report(const Controller_t* const cs,         // controller state
                                const Button_Function_t* const km,    // keymap
                                Composite_Report_t* const cr,         // report
                                const uint8_t report_id)
{
  cr->report_id = report_id;
  uint8_t registerd_buttons = 0;

  // Iterate over controller state buttons.
  for (uint8_t i = 0; i < 32; i++)
  {
    if (cs->buttons & (1 << i))
    {
      // Translate the button (which stems from the GPIO number) to the keymap function.
      int8_t map[32];
      memset(map, -1, 32);
      map[4] = 0;
      map[5] = 1;
      map[6] = 2;
      map[7] = 3;
      map[8] = 4;
      map[9] = 5;
      map[10] = 6;
      map[11] = 7;

      // The map_index will be -1 if the button isn't mapped.
      const int8_t map_index = map[i];

      if (map_index >= 0 && map_index < CFG_BUTTONS_COUNT)
      {
        switch (km[map_index].role)
        {
          case KEY:
            cr->keyboard.keys_down[registerd_buttons++] = km[map_index].key;
            break;
          case MODIFIER:
            cr->keyboard.modifiers |= km[map_index].modifiers_mask;
            break;
          case MACRO: break;
        }
      }
      else
      {
        snprintf(print_buf, PRINT_BUF_SIZE, "Fetching record from keymap with invalid index: %d\r\n", map_index);
        TRACE_PRINT(0, print_buf);
      }
    }
  }
}

// systick counter gets enabled in the hid_control_request().
// That's because polling the controller makes sense if USB works well.
void sys_tick_handler(void)
{
  systick_counter += 1;
  char buf[128];

  // Every third systick establish consensus state and if it differs from the last one, push the
  // state to the host.
  if (systick_counter % 3 == 0)
  {
    hw_establish_consensus_state();
    if (hw_state_changed())
    {
      Controller_t consensus = hw_get_consensus_state();
      hw_controller_state_string(buf, 128, consensus);
      TRACE_PRINT(0, buf);

      // Translate the hardware state into a HID report.
      Composite_Report_t report = {};
      controller_state_to_report(&consensus, keymap, &report, 2U);

      usbd_ep_write_packet(usbd_dev, ENDPOINT_ADDRESS, (void*)&report, 9U);
    }
  }
  else
  {
    hw_save_new_controller_state();
  }
  gpio_toggle(GPIOC, GPIO13);
}

void tim2_isr(void)
{
  timer_enable_counter(TIM2);
}
