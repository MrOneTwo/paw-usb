# Paw controller via USB
# PAW USB

This project is the firmware for a controller focused on helping CG artists. Imagine one handed
keyboard with some unique knobs and trinkets which help CG artists make art faster and more ergonomically.

Originally this project started as a BLE controller. Unfortunately Windoows doesn't play well with
the current BLE implementation. Since the protocol isn't the main point of this project (unique input
methods for artists are), I've pivoted to using **STM32** with **USB**.

Using:

- **STM32F103C8** - *Bluepill* board
- **libopencm3** - FOSS framework for STM32


## Building

Be sure to install [just](https://github.com/casey/just#running-recipes-at-the-end-of-a-recipe) tool if you want the 'easy-peasy' process setup process.

```sh
git clone https://gitlab.com/MrOneTwo/paw-usb.git && cd paw-usb
git submodule update --init --recursive
just setup_toolchain
make
```

After that you should be able to flash the board with:

```sh
just flash
```

If you want to see trace output, open two terminal windows. One should do this:

```sh
bash dbg_server.sh external
```

The second should do this (you have to have the UART to USB dongle connected to pin SWO PB3):

```sh
picocom /dev/ttyUSB0 -b 2000000
```
